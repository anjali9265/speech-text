# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
import voice_to_text.speech as sr
from rest_framework.views import APIView
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
class Convert(APIView):
    def post(self, request,*args):
        r = sr.Recognizer()
        audioFile = sr.AudioFile('C:/Users/anjali/Desktop/voice_to_text/voice_to_text/speech/test.wav')
        with audioFile as source:
            audio = r.record(source)
        text = r.recognize_google(audio)
        return HttpResponse(text)
